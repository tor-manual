<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
</head>
<body>

<h1 id="the-short-user-manual">The Short User Manual</h1>
<p>This user manual contains information about how to download Tor, how to use it, and what to do if Tor is unable to connect to the network. If you can't find the answer to your question in this document, email help@rt.torproject.org.</p>
<p>Please note that we are providing support on a voluntary basis, and we get a lot of emails every single day. There is no need to worry if we don't get back to you straight away.</p>
<h2 id="how-tor-works">How Tor works</h2>
<p>Tor is a network of virtual tunnels that allows you to improve your privacy and security on the Internet. Tor works by sending your traffic through three random servers (also known as <em>relays</em>) in the Tor network, before the traffic is sent out onto the public Internet.</p>
<p>The image above illustrates a user browsing to different websites over Tor. The green monitors represent relays in the Tor network, while the three keys represent the layers of encryption between the user and each relay.</p>
<p>Tor will anonymize the origin of your traffic, and it will encrypt everything between you and the Tor network. Tor will also encrypt your traffic inside the Tor network, but it cannot encrypt your traffic between the Tor network and its final destination.</p>
<p>If you are communicating sensitive information, for example when logging on to a website with a username and password, make sure that you are using HTTPS (e.g. <strong>https</strong>://torproject.org/, not <strong>http</strong>://torproject.org/).</p>
<h2 id="how-to-download-tor">How to download Tor</h2>
<p>The bundle we recommend to most users is the <a href="https://www.torproject.org/projects/torbrowser.html">Tor Browser Bundle</a>. This bundle contains a browser preconfigured to safely browse the Internet through Tor, and requires no installation. You download the bundle, extract the archive, and start Tor.</p>

<p>There are two different ways to get hold of the Tor software. You can either browse to the <a href="https://www.torproject.org/">Tor Project website</a> and download it there, or you can use GetTor, the email autoresponder.</p>
<h3 id="how-to-get-tor-via-email">How to get Tor via email</h3>
<p>To receive the English Tor Browser Bundle for Windows, send an email to gettor@torproject.org with <strong>windows</strong> in the body of the message. You can leave the subject blank.</p>
<p>You can also request the Tor Browser Bundle for Mac OS X (write <strong>macos-i386</strong>), and Linux (write <strong>linux-i386</strong> for 32-bit systems or <strong>linux-x86_64</strong> for 64-bit systems).</p>

<p>If you want a translated version of Tor, write <strong>help</strong> instead. You will then receive an email with instructions and a list of available languages.</p>
<p><strong>Note</strong>: The Tor Browser Bundles for Linux and Mac OS X are rather large, and you will not be able to receive any of these bundles with a Gmail, Hotmail or Yahoo account. If you cannot receive the bundle you want, send an email to help@rt.torproject.org and we will give you a list of website mirrors to use.</p>
<h3 id="tor-for-smartphones">Tor for smartphones</h3>
<p>You can get Tor on your Android device by installing the package named <em>Orbot</em>. For information about how to download and install Orbot, please see the <a href="https://www.torproject.org/docs/android.html.en">Tor Project website</a>.</p>
<p>We also have experimental packages for <a href="https://www.torproject.org/docs/N900.html.en">Nokia Maemo/N900</a> and <a href="http://sid77.slackware.it/iphone/">Apple iOS</a>.</p>

<h3 id="how-to-verify-that-you-have-the-right-version">How to verify that you have the right version</h3>
<p>Before running the Tor Browser Bundle, you should make sure that you have the right version.</p>
<p>The software you receive is accompanied by a file with the same name as the bundle and the extension <strong>.asc</strong>. This .asc file is a GPG signature, and will allow you to verify the file you've downloaded is exactly the one that we intended you to get.</p>
<p>Before you can verify the signature, you will have to download and install GnuPG:</p>
<p><strong>Windows</strong>: <a href="http://gpg4win.org/download.html">http://gpg4win.org/download.html</a><br /><strong>Mac OS X</strong>: <a href="http://macgpg.sourceforge.net/">http://macgpg.sourceforge.net/</a><br /><strong>Linux</strong>: Most Linux distributions come with GnuPG preinstalled.</p>

<p>Please note that you may need to edit the paths and the commands used below to get it to work on your system.</p>
<p>Erinn Clark signs the Tor Browser Bundles with key 0x63FEE659. To import Erinn's key, run:</p>
<pre><code>gpg --keyserver hkp://keys.gnupg.net  --recv-keys 0x63FEE659
</code></pre>
<p>After importing the key, verify that the fingerprint is correct:</p>
<pre><code>gpg  --fingerprint 0x63FEE659
</code></pre>
<p>You should see:</p>
<pre><code>pub   2048R/63FEE659 2003-10-16
      Key fingerprint = 8738 A680 B84B 3031 A630  F2DB 416F 0610 63FE E659
uid                  Erinn Clark &lt;erinn@torproject.org&gt;
uid                  Erinn Clark &lt;erinn@debian.org&gt;

uid                  Erinn Clark &lt;erinn@double-helix.org&gt;
sub   2048R/EB399FD7 2003-10-16
</code></pre>
<p>To verify the signature of the package you downloaded, run the following command:</p>
<pre><code>gpg --verify tor-browser-2.2.33-2_en-US.exe.asc tor-browser-2.2.33-2_en-US.exe
</code></pre>
<p>The output should say <em>&quot;Good signature&quot;</em>. A bad signature means that the file may have been tampered with. If you see a bad signature, send details about where you downloaded the package from, how you verified the signature, and the output from GnuPG in an email to help@rt.torproject.org.</p>
<p>Once you have verified the signature and seen the <em>&quot;Good signature&quot;</em> output, go ahead and extract the package archive. You should then see a directory similar to <strong>tor-browser_en-US</strong>. Inside that directory is another directory called <strong>Docs</strong>, which contains a file called <strong>changelog</strong>. You want to make sure that the version number on the top line of the changelog file matches the version number in the filename.</p>

<h3 id="how-to-use-the-tor-browser-bundle">How to use the Tor Browser Bundle</h3>
<p>After downloading the Tor Browser Bundle and extracting the package, you should have a directory with a few files in it. One of the files is an executable called &quot;Start Tor Browser&quot; (or &quot;start-tor-browser&quot;, depending on your operating system).</p>
<p>When you start the Tor Browser Bundle, you will first see Vidalia start up and connect you to the Tor network. After that, you will see a browser confirming that you are now using Tor. This is done by displaying <a href="https://check.torproject.org/">https://check.torproject.org/</a>. You can now browse the Internet through Tor.</p>
<p><em>Please note that it is important that you use the browser that comes with the bundle, and not your own browser.</em></p>
<h3 id="what-to-do-when-tor-does-not-connect">What to do when Tor does not connect</h3>

<p>Some users will notice that Vidalia gets stuck when trying to connect to the Tor network. If this happens, make sure that you are connected to the Internet. If you need to connect to a proxy server, see <em>How to use an open proxy</em> below.</p>
<p>If your normal Internet connection is working, but Tor still can't connect to the network, try the following; open the Vidalia control panel, click on <em>Message Log</em> and select the <em>Advanced</em> tab. It may be that Tor won't connect because:</p>
<p><strong>Your system clock is off</strong>: Make sure that the date and time on your system is correct, and restart Tor. You may need to synchronize your system clock with an Internet time server.</p>
<p><strong>You are behind a restrictive firewall</strong>: To tell Tor to only try port 80 and port 443, open the Vidalia control panel, click on <em>Settings</em> and <em>Network</em>, and tick the box that says <em>My firewall only lets me connect to certain ports</em>.</p>

<p><strong>Your anti-virus program is blocking Tor</strong>: Make sure that your anti-virus program is not preventing Tor from making network connections.</p>
<p>If Tor still doesn't work, it's likely that your Internet Service Provider (ISP) is blocking Tor. Very often this can be worked around with <strong>Tor bridges</strong>, hidden relays that aren't as easy to block.</p>
<p>If you need help with figuring out why Tor can't connect, send an email to help@rt.torproject.org and include the relevant parts from the log file.</p>
<h3 id="how-to-find-a-bridge">How to find a bridge</h3>
<p>To use a bridge, you will first have to locate one; you can either browse to <a href="https://bridges.torproject.org/">bridges.torproject.org</a>, or you can send an email to bridges@torproject.org. If you do send an email, please make sure that you write <strong>get bridges</strong> in the body of the email. Without this, you will not get a reply. Note that you need to send this email from either a gmail.com or a yahoo.com address.</p>

<p>Configuring more than one bridge address will make your Tor connection more stable, in case some of the bridges become unreachable. There is no guarantee that the bridge you are using now will work tomorrow, so you should make a habit of updating your list of bridges every so often.</p>
<h3 id="how-to-use-a-bridge">How to use a bridge</h3>
<p>Once you have a set of bridges to use, open the Vidalia control panel, click on <em>Settings</em>, <em>Network</em> and tick the box that says <em>My ISP blocks connections to the Tor network</em>. Enter the bridges in the box below, hit <em>OK</em> and start Tor again.</p>
<h3 id="how-to-use-an-open-proxy">How to use an open proxy</h3>

<p>If using a bridge does not work, try configuring Tor to use any HTTPS or SOCKS proxy to get access to the Tor network. This means even if Tor is blocked by your local network, open proxies can be safely used to connect to the Tor Network and on to the uncensored Internet.</p>
<p>The steps below assume you have a functional Tor/Vidalia configuration, and you have found a list of HTTPS, SOCKS4, or SOCKS5 proxies.</p>
<ol style="list-style-type: decimal">
<li>Open the Vidalia control panel, click on <em>Settings</em>.</li>
<li>Click <em>Network</em>. Select <em>I use a proxy to access the Internet</em>.</li>
<li>On the <em>Address</em> line, enter the open proxy address. This can be a hostname or an IP Address.</li>

<li>Enter the port for the proxy.</li>
<li>Generally, you do not need a username and password. If you do, enter the information in the proper fields.</li>
<li>Choose the <em>Type</em> of proxy you are using, whether HTTP/HTTPS, SOCKS4, or SOCKS5.</li>
<li>Push the <em>OK</em> button. Vidalia and Tor are now configured to use a proxy to access the rest of the Tor network.</li>
</ol>
<h2 id="frequently-asked-questions">Frequently Asked Questions</h2>

<p>This section will answer some of the most common questions. If your question is not mentioned here, please send an email to help@rt.torproject.org.</p>
<h3 id="unable-to-extract-the-archive">Unable to extract the archive</h3>
<p>If you are using Windows and find that you cannot extract the archive, download and install <a href="http://www.7-zip.org/">7-Zip</a>.</p>
<p>If you are unable to download 7-Zip, try to rename the file from .z to .zip and use winzip to extract the archive. Before renaming the file, tell Windows to show file extensions:</p>
<h4 id="windows-xp">Windows XP</h4>
<ol style="list-style-type: decimal">
<li>Open <em>My Computer</em></li>
<li>Click on <em>Tools</em> and choose <em>Folder Options...</em> in the menu</li>

<li>Click on the <em>View</em> tab</li>
<li>Uncheck <em>Hide extensions for known file types</em> and click <em>OK</em></li>
</ol>
<h4 id="windows-vista">Windows Vista</h4>
<ol style="list-style-type: decimal">
<li>Open <em>Computer</em></li>

<li>Click on <em>Organize</em> and choose <em>Folder and search options</em> in the menu</li>
<li>Click on the <em>View</em> tab</li>
<li>Uncheck <em>Hide extensions for known file types</em> and click <em>OK</em></li>

</ol>
<h4 id="windows-7">Windows 7</h4>
<ol style="list-style-type: decimal">
<li>Open <em>Computer</em></li>
<li>Click on <em>Organize</em> and choose <em>Folder and search options</em> in the menu</li>
<li>Click on the <em>View</em> tab</li>

<li>Uncheck <em>Hide extensions for known file types</em> and click <em>OK</em></li>
</ol>
<h3 id="vidalia-asks-for-a-password">Vidalia asks for a password</h3>
<p>You should not have to enter a password when starting Vidalia. If you are prompted for one, you are likely affected by one of these problems:</p>
<p><strong>You are already running Vidalia and Tor</strong>: For example, this situation can happen if you installed the Vidalia bundle and now you're trying to run the Tor Browser Bundle. In that case, you will need to close the old Vidalia and Tor before you can run this one.</p>
<p><strong>Vidalia crashed, but left Tor running</strong>: If the dialog that prompts you for a control password has a Reset button, you can click the button and Vidalia will restart Tor with a new random control password. If you do not see a Reset button, or if Vidalia is unable to restart Tor for you; go into your process or task manager, and terminate the Tor process. Then use Vidalia to restart Tor.</p>

<p>For more information, see the <a href="https://torproject.org/docs/faq.html#VidaliaPassword">FAQ</a> on the Tor Project website.</p>
<h3 id="flash-does-not-work">Flash does not work</h3>
<p>For security reasons, Flash, Java, and other plugins are currently disabled for Tor. Plugins operate independently from Firefox and can perform activity on your computer that ruins your anonymity.</p>
<p>Most YouTube videos work with HTML5, and it is possible to view these videos over Tor. You need to join the <a href="https://www.youtube.com/html5">HTML5 trial</a> on the YouTube website before you can use the HTML5 player.</p>
<p>Note that the browser will not remember that you joined the trial once you close it, so you will need to re-join the trial the next time you run the Tor Browser Bundle.</p>
<p>Please see the <a href="https://www.torproject.org/torbutton/torbutton-faq.html#noflash">Torbutton FAQ</a> for more information.</p>

<h3 id="i-want-to-use-another-browser">I want to use another browser</h3>
<p>For security reasons, we recommend that you only browse the web through Tor using the Tor Browser Bundle. It is technically possible to use Tor with other browsers, but by doing so you open yourself up to potential attacks.</p>
<h3 id="why-tor-is-slow">Why Tor is slow</h3>
<p>Tor can sometimes be a bit slower than your normal Internet connection. After all, your traffic is sent through many different countries, sometimes across oceans around the world!</p>

</body>
</html>
